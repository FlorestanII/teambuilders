package me.florestanii.teambuilders.listener;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.TeamBuildersArena;
import me.florestanii.teambuilders.arena.TeamBuildersArenaState;
import me.florestanii.teambuilders.util.components.PluginComponentBase;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ProtectionHandler extends PluginComponentBase implements Listener{

	private final TeamBuilders tb;
	
	public ProtectionHandler(TeamBuilders tb) {
		this.tb = tb;
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING) {
				event.setCancelled(true);
			} else {
				if (!arena.getTeam(event.getPlayer()).getBuildarea().isInArea(event.getBlock().getLocation())) {
					event.setCancelled(true);
				} else {
					event.setCancelled(false);
				}
			}
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING) {
				event.setCancelled(true);
			} else {
				if (!arena.getTeam(event.getPlayer()).getBuildarea().isInArea(event.getBlock().getLocation())) {
					event.setCancelled(true);
				} else {
					event.setCancelled(false);
				}
			}
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockInteract(PlayerInteractEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			if (arena.getState() != TeamBuildersArenaState.BUILDING) {
				event.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		
		TeamBuildersArena arena = tb.getArena((Player)event.getEntity());
		
		if (arena != null) {
			
			event.setCancelled(true);
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event) {
		
		if (event.getEntityType() != EntityType.PLAYER) {
			return;
		}
		Player player = (Player) event.getEntity();
		TeamBuildersArena arena = tb.getArena(player);
		
		
		if (arena != null) {
			
			event.setCancelled(true);
			
		}
		
	}
	
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING && arena.getState() != TeamBuildersArenaState.PREPARING) {
				
				event.setCancelled(true);
				
			}
			
		}
		
	}
	
	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING && arena.getState() != TeamBuildersArenaState.PREPARING) {
				
				event.setCancelled(true);
				
			}
			
		}
		
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		
		TeamBuildersArena arena = tb.getArena((Player)event.getWhoClicked());
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING && arena.getState() != TeamBuildersArenaState.PREPARING) {
				
				event.setCancelled(true);
				
			}
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		
		if (event.getDamager().getType() != EntityType.PLAYER) {
			return;
		}
		
		Player player = (Player) event.getDamager();
		
		TeamBuildersArena arena = tb.getArena(player);
		
		if (arena != null) {
			
			if (arena.getState() != TeamBuildersArenaState.BUILDING) {
				
				event.setCancelled(true);
				
			}
			
		}
		
	}
	
}
