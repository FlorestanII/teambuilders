package me.florestanii.teambuilders.listener;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.TeamBuildersArena;
import me.florestanii.teambuilders.util.components.PluginComponentBase;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerInventoryBackupHandler extends PluginComponentBase implements Listener{

	private final TeamBuilders tb;
	
	public PlayerInventoryBackupHandler(TeamBuilders tb) {
		this.tb = tb;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		
		TeamBuildersArena arena = tb.getArena(event.getPlayer());
		
		if (arena != null) {
			
			arena.leavePlayer(event.getPlayer());
			
		}
		
	}
	
}
