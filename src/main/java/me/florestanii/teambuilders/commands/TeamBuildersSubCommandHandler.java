package me.florestanii.teambuilders.commands;

import me.florestanii.teambuilders.util.commands.SubCommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class TeamBuildersSubCommandHandler extends SubCommandHandler{
	public TeamBuildersSubCommandHandler(String parentCommand) {
        super(parentCommand);
    }

    @Override
    protected final void onInvalidCommand(CommandSender sender) {
        sender.sendMessage(ChatColor.RED + "Unknown command. Type "  + ChatColor.GOLD + "/teambuilders help " + ChatColor.RED + "for a full list of the commands.");
    }

    @Override
    protected final void onPermissionDenied(CommandSender sender, Command command, String[] args) {
        sender.sendMessage(ChatColor.RED + "You have no permission to use this command.");
    }

    @Override
    protected final void sendHelpLine(CommandSender sender, me.florestanii.teambuilders.util.commands.Command command) {
        if (command.value().length > 0) {
        	sender.sendMessage(ChatColor.RED + "/" + getParentCommand() + " " + command.value()[0] + " - " + command.description());
        } else {
        	sender.sendMessage(ChatColor.RED + "/" + getParentCommand() + " - " + command.description());
        }
    }

    @Override
    protected final void sendUsageHelp(CommandSender sender, me.florestanii.teambuilders.util.commands.Command command) {
        StringBuilder usageText = new StringBuilder();
        if (command.usage().length > 0) {
            usageText.append(command.description()).append("\n");
            for (String usage : command.usage()) {
                usageText.append("/" + getParentCommand()).append(usage).append("\n");
            }
        } else {
            usageText.append("/" + getParentCommand());
            if (command.value().length > 0) {
                usageText.append(" " + command.value()[0]);
            }
            usageText.append(" - " + command.description());
        }
        sender.sendMessage(usageText.toString());
    }
}
