package me.florestanii.teambuilders.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.TeamBuildersArena;
import me.florestanii.teambuilders.arena.TeamBuildersArenaState;
import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;
import me.florestanii.teambuilders.util.commands.Command;
import me.florestanii.teambuilders.util.commands.CommandHandler;

public class ArenaCommands implements CommandHandler{

	private final TeamBuilders tb;
	
	public ArenaCommands(TeamBuilders tb) {
		this.tb = tb;
	}
	
	@Command(
			value= "list",
			permission = "teambuilders.admin.list",
            description = "Show a full list of all arenas.",
            usage = " list",
            min = 0,
            max = 0)
	public void list(Player player) {
		
		player.sendMessage(ChatColor.YELLOW + "There are " + tb.getArenas().size() + " arenas:");
		
		for (TeamBuildersArena arena : tb.getArenas()) {
			player.sendMessage(ChatColor.RED + arena.getName());
		}
		
		player.sendMessage(ChatColor.YELLOW + "Type " + ChatColor.RED + "/teambuilders info <arena> " + ChatColor.YELLOW + " for more informations.");
		
	}
	
	@Command(
			value= "info",
			permission = "teambuilders.admin.info",
            description = "Show details about a arena.",
            usage = " info <arena>",
            min = 1,
            max = 1)
	public void info(Player player, String[] args) {
		
		TeamBuildersArena arena = tb.getArena(args[0]);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "There is no arena with this name!");
			player.sendMessage(ChatColor.YELLOW + "Type " + ChatColor.RED + "/teambuilders list " + ChatColor.YELLOW + "for a full list.");
			return;
		}
		
		player.sendMessage(ChatColor.YELLOW + "Informations about arena " + ChatColor.RED + arena.getName() + ": ");
		
		if (arena.getName() != null) {
			player.sendMessage(ChatColor.YELLOW + "Name: " + ChatColor.RED + arena.getName());
		}
		
		if (arena.getLobbySpawn() != null) {
			player.sendMessage(ChatColor.YELLOW + "Lobby Location: " + arena.getLobbySpawn().getX() + " " + arena.getLobbySpawn().getY() + " " + arena.getLobbySpawn().getZ());
		}
		
		player.sendMessage(ChatColor.YELLOW + "Maximal Fly Height: " + ChatColor.RED + arena.getFlyHeight());
		player.sendMessage(ChatColor.YELLOW + "Maximal Build Height: " + ChatColor.RED + arena.getBuildHeight());
		player.sendMessage(ChatColor.YELLOW + "Maximal Build Depth: " + ChatColor.RED + arena.getBuildDepth());
		
		player.sendMessage(ChatColor.YELLOW + "Team Size: " + ChatColor.RED + arena.getTeamSize());
		player.sendMessage(ChatColor.YELLOW + "Minimum Players: " + ChatColor.RED + arena.getMinPlayers());

		if (arena.getTeams().size() > 0) {
			
			player.sendMessage(ChatColor.YELLOW + "Teams:");
			
			for (TeamBuildersTeam team : arena.getTeams()) {
				player.sendMessage(ChatColor.YELLOW + "  Name: " + team.getChatColor() + team.getTeamName());
				player.sendMessage(ChatColor.YELLOW + "  Chat Color: " + team.getChatColor() + team.getChatColor().name());
				
				if (team.getSpawn() != null) {
					player.sendMessage(ChatColor.YELLOW + "  Spawn: " + ChatColor.RED + team.getSpawn().getX() + " " + team.getSpawn().getY() + " " + team.getSpawn().getZ());
				}

				if (team.getBuildarea() != null) {
					player.sendMessage(ChatColor.YELLOW + "  Build Area: " + ChatColor.RED + team.getBuildarea().toString());
				}
			}
			
		}
		
	}
	
	@Command(
			value= "join",
			permission = "teambuilders.play",
            description = "Join the arena.",
            usage = " join <arena>",
            min = 1,
            max = 1)
	public void join(Player player, String[] args) {
		
		TeamBuildersArena arena = tb.getArena(args[0]);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "There is no arena with this name!");
			return;
		}
		
		if (tb.getArena(player) != null) {
			player.sendMessage(ChatColor.DARK_RED + "You are already in an arena!");
			return;
		}
		
		arena.joinPlayer(player);
		
	}
	
	@Command(
			value= "leave",
			permission = "teambuilders.play",
            description = "Leave the arena.",
            usage = " leave",
            min = 0,
            max = 0)
	public void leave(Player player) {
		
		TeamBuildersArena arena = tb.getArena(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You are in no arena!");
			return;
		}
		
		arena.leavePlayer(player);
		
	}
	
	@Command(
			value= "start",
			permission = "teambuilders.play.start",
            description = "Set the countdown of the arena to 5.",
            usage = " start",
            min = 0,
            max = 0)
	public void start(Player player) {
		
		TeamBuildersArena arena = tb.getArena(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You are in no arena!");
			return;
		}
		
		if (arena.getState() != TeamBuildersArenaState.LOBBY) {
			player.sendMessage(ChatColor.DARK_RED + "This arena is already started!");
			return;
		}
		
		arena.getLobby().setCountdown(5);
		
	}

	@Command(
			value= "addword",
			permission = "teambuilders.admin",
            description = "Add a new word.",
            usage = " addword <word>",
            min = 1,
            max = 1)
	public void addword(Player player, String[] args) {
		
		tb.addWord(args[0]);
		player.sendMessage(ChatColor.YELLOW + "Add '" + args[0] + "'");
		
	}
	
}
