package me.florestanii.teambuilders.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.ArenaSetupCache;
import me.florestanii.teambuilders.arena.BuildArea;
import me.florestanii.teambuilders.arena.TeamBuildersArena;
import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;
import me.florestanii.teambuilders.util.commands.Command;
import me.florestanii.teambuilders.util.commands.CommandHandler;

public class ArenaSetupCommands implements CommandHandler{

	private final TeamBuilders tb;
	
	public ArenaSetupCommands(TeamBuilders tb) {
		this.tb = tb;
	}
	
	@Command(
            value = "start",
            permission = "teambuilders.admin.setup",
            description = "Start the setup of a new arena.",
            usage = " start <arena name>",
            min = 0,
            max = 1)
    public void startSetup(Player player, String[] args) {
		
        TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
        
        arena = new TeamBuildersArena(tb);
        	
        if (args.length >= 1) {
        	String arenaName = args[0];
            arena.setName(arenaName);
            	
            player.sendMessage("Start arena setup! (" + arenaName + ")");
        } else {
        	player.sendMessage("Start arena setup!");
        }
        
        ArenaSetupCache.setCache(player, arena);
        
    }
	
	@Command(
			value= "name",
			permission = "teambuilders.admin.setup",
            description = "Set the arena name.",
            usage = " name <arena name>",
            min = 1,
            max = 1)
	public void setName(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String arenaName = args[0];
		arena.setName(arenaName);
		
		player.sendMessage(ChatColor.YELLOW + "Set arena name to: " + arenaName);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "info",
			permission = "teambuilders.admin.setup",
            description = "Show something about your arena.",
            usage = " info",
            min = 0,
            max = 0)
	public void info(Player player) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		player.sendMessage(ChatColor.YELLOW + "Informations about arena: ");
		
		if (arena.getName() != null) {
			player.sendMessage(ChatColor.YELLOW + "Name: " + ChatColor.RED + arena.getName());
		}
		
		if (arena.getLobbySpawn() != null) {
			player.sendMessage(ChatColor.YELLOW + "Lobby Location: " + arena.getLobbySpawn().getX() + " " + arena.getLobbySpawn().getY() + " " + arena.getLobbySpawn().getZ());
		}
		
		player.sendMessage(ChatColor.YELLOW + "Maximal Fly Height: " + ChatColor.RED + arena.getFlyHeight());
		player.sendMessage(ChatColor.YELLOW + "Maximal Build Height: " + ChatColor.RED + arena.getBuildHeight());
		player.sendMessage(ChatColor.YELLOW + "Maximal Build Depth: " + ChatColor.RED + arena.getBuildDepth());
		
		player.sendMessage(ChatColor.YELLOW + "Team Size: " + ChatColor.RED + arena.getTeamSize());
		player.sendMessage(ChatColor.YELLOW + "Minimum Players: " + ChatColor.RED + arena.getMinPlayers());
		
		if (arena.getTeams().size() > 0) {
			
			player.sendMessage(ChatColor.YELLOW + "Teams:");
			
			for (TeamBuildersTeam team : arena.getTeams()) {
				player.sendMessage(ChatColor.YELLOW + "  Name: " + team.getChatColor() + team.getTeamName());
				player.sendMessage(ChatColor.YELLOW + "  Chat Color: " + team.getChatColor() + team.getChatColor().name());
				
				if (team.getSpawn() != null) {
					player.sendMessage(ChatColor.YELLOW + "  Spawn: " + ChatColor.RED + team.getSpawn().getX() + " " + team.getSpawn().getY() + " " + team.getSpawn().getZ());
				}
				
				if (team.getBuildarea() != null) {
					player.sendMessage(ChatColor.YELLOW + "  Build Area: " + ChatColor.RED + team.getBuildarea().toString());
				}
				
			}
			
		}
		
	}
	
	@Command(
			value= "done",
			permission = "teambuilders.admin.setup",
            description = "Complete the setup.",
            usage = " done",
            min = 0,
            max = 0)
	public void done(Player player) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		if (!arena.isReady()) {
			player.sendMessage(ChatColor.DARK_RED + "Arena isn't ready!");
			return;
		}
		
		tb.addArena(arena);
		tb.saveArena(arena);
		player.sendMessage(ChatColor.YELLOW + "Complete the arena setup");
		
		ArenaSetupCache.removeCache(player);
		
	}

	@Command(
			value= "lobby",
			permission = "teambuilders.admin.setup",
            description = "Set the lobby location.",
            usage = " lobby",
            min = 0,
            max = 0)
	public void lobby(Player player) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		arena.setLobbySpawn(player.getLocation());
		
		player.sendMessage(ChatColor.YELLOW + "Set lobby location.");
		
		ArenaSetupCache.setCache(player, arena);
		
	}
	
	@Command(
			value= "addteam",
			permission = "teambuilders.admin.setup",
            description = "Add a new team",
            usage = " addteam <team default name> <team chat color>",
            min = 2,
            max = 2)
	public void addTeam(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String teamName = args[0];
		
		ChatColor chatColor = null;
		
		try {
			chatColor = ChatColor.valueOf(args[1].toUpperCase());
		} catch (Exception e) {
			player.sendMessage(ChatColor.YELLOW + args[1] + " isn't a chat color!");
		}
		
		TeamBuildersTeam newTeam = new TeamBuildersTeam(tb, arena, teamName);
		newTeam.setChatColor(chatColor);
		
		arena.addTeam(newTeam);
		
		player.sendMessage(ChatColor.YELLOW + "Add team " + chatColor + teamName);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "teamsize",
			permission = "teambuilders.admin.setup",
            description = "Set the maximal team size.",
            usage = " teamsize <maximal team size>",
            min = 1,
            max = 1)
	public void setTeamSize(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String teamSize = args[0];
		
		try {
			arena.setTeamSize(Integer.valueOf(teamSize));
		} catch(Exception e) {
			player.sendMessage(ChatColor.YELLOW + teamSize + ChatColor.DARK_RED + " isn't a number!");
		}
		
		player.sendMessage(ChatColor.YELLOW + "Set maximal team size to: " + ChatColor.RED + teamSize);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "flyheight",
			permission = "teambuilders.admin.setup",
            description = "Set the maximal fly height.",
            usage = " flyheight <maximal fly height>",
            min = 1,
            max = 1)
	public void setFlyHeight(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String flyHeight = args[0];
		
		try {
			arena.setFlyHeight(Integer.valueOf(flyHeight));
		} catch(Exception e) {
			player.sendMessage(ChatColor.YELLOW + flyHeight + ChatColor.DARK_RED + " isn't a number!");
		}
		
		player.sendMessage(ChatColor.YELLOW + "Set maximal fly height to: " + ChatColor.RED + flyHeight);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "buildheight",
			permission = "teambuilders.admin.setup",
            description = "Set the maximal build height.",
            usage = " buildheight <maximal build height>",
            min = 1,
            max = 1)
	public void setBuildHeight(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String buildHeight = args[0];
		
		try {
			arena.setBuildHeight(Integer.valueOf(buildHeight));
		} catch(Exception e) {
			player.sendMessage(ChatColor.YELLOW + buildHeight + ChatColor.DARK_RED + " isn't a number!");
		}
		
		player.sendMessage(ChatColor.YELLOW + "Set maximal build height to: " + ChatColor.RED + buildHeight);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "builddepth",
			permission = "teambuilders.admin.setup",
            description = "Set the maximal build depth.",
            usage = " builddepth <maximal build depth>",
            min = 1,
            max = 1)
	public void setBuildDepth(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String buildDepth = args[0];
		
		try {
			arena.setBuildDepth(Integer.valueOf(buildDepth));
		} catch(Exception e) {
			player.sendMessage(ChatColor.YELLOW + buildDepth + ChatColor.DARK_RED + " isn't a number!");
		}
		
		player.sendMessage(ChatColor.YELLOW + "Set maximal build depth to: " + ChatColor.RED + buildDepth);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "teamspawn",
			permission = "teambuilders.admin.setup",
            description = "Set the spawn of a team at your current location.",
            usage = " teamspawn <team>",
            min = 1,
            max = 1)
	public void setTeamSpawn(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		String teamName = args[0];
				
		if (arena.getTeam(teamName) == null) {
			player.sendMessage(ChatColor.DARK_RED + "Unknown team!");
			return;
		}
		
		arena.getTeam(teamName).setSpawn(player.getLocation());
		
		player.sendMessage(ChatColor.YELLOW + "Set spawn of team: " + ChatColor.RED + teamName);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
	@Command(
			value= "buildarea",
			permission = "teambuilders.admin.setup",
            description = "Add a build area for a team.",
            usage = " buildarea <team>",
            min = 1,
            max = 1)
	public void addBuildArea(final Player player, final String[] args) {
		final TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		final TeamBuildersTeam team = arena.getTeam(args[0]);
		
		if (team == null) {
			player.sendMessage(ChatColor.DARK_RED + "There is no team with this name!");
			return;
		}
		
		final BuildArea buildarea = new BuildArea(arena, team);
		
		player.sendMessage(ChatColor.YELLOW + "Click on the first corner!");
		
		tb.getServer().getPluginManager().registerEvents(new Listener() {
			
			@EventHandler(priority=EventPriority.HIGHEST)
			public void onBlockInteract(PlayerInteractEvent event) {
				
				if (event.getPlayer().getUniqueId().equals(player.getUniqueId())) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
						Location loc = event.getClickedBlock().getLocation();
						
						event.setCancelled(true);
						
						if (buildarea.getPos1() == null) {
							buildarea.setPos1(loc);
							player.sendMessage(ChatColor.YELLOW + "Click now on the other corner!");
						} else if (buildarea.getPos2() == null) {
							buildarea.setPos2(loc);
							PlayerInteractEvent.getHandlerList().unregister(this);
							arena.getTeam(args[0]).setBuildarea(buildarea);
							player.sendMessage(ChatColor.YELLOW + "Add build area " + buildarea.toString());
							ArenaSetupCache.setCache(player, arena);
						}
						
					}
				}
				
			}
			
		}, tb);
		
		ArenaSetupCache.setCache(player, arena);
		
	}
	
	@Command(
			value= "minplayers",
			permission = "teambuilders.admin.setup",
            description = "Set the amount of players that is required, to start the arena.",
            usage = " minplayers <mineplayers>",
            min = 1,
            max = 1)
	public void setMinPlayers(Player player, String[] args) {
		TeamBuildersArena arena = ArenaSetupCache.getCacheOfPlayer(player);
		
		if (arena == null) {
			player.sendMessage(ChatColor.DARK_RED + "You haven't started the setup!");
			return;
		}
		
		try {
			arena.setMinPlayers(Integer.parseInt(args[0]));
		} catch (Exception e) {
			player.sendMessage(ChatColor.DARK_RED + args[0] + " is no allowed number");
			return;
		}
		
		player.sendMessage(ChatColor.YELLOW + "Set minimum player to: " + ChatColor.RED + args[0]);
		
		ArenaSetupCache.setCache(player, arena);
        
	}
	
}
