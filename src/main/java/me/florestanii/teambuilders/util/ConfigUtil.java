package me.florestanii.teambuilders.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

public class ConfigUtil {

	public static void saveLocation(ConfigurationSection sec, Location loc) {
		sec.set("world", loc.getWorld().getName());
		sec.set("x", loc.getX());
		sec.set("y", loc.getY());
		sec.set("z", loc.getZ());
		sec.set("pitch", loc.getPitch());
		sec.set("yaw", loc.getYaw());
	}
	
	public static Location loadLocation(ConfigurationSection sec) {
		World world = Bukkit.getServer().getWorld(sec.getString("world"));
		double x = sec.getDouble("x");
		double y = sec.getDouble("y");
		double z = sec.getDouble("z");
		float pitch = (float)sec.getDouble("pitch");
		float yaw = (float)sec.getDouble("yaw");
		return new Location(world, x, y, z, yaw, pitch);
	}
	
}
