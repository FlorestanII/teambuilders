package me.florestanii.teambuilders.util.components;

import org.bukkit.plugin.java.JavaPlugin;

public interface PluginComponent {
    void activateFor(JavaPlugin craftenServerPlugin);
}
