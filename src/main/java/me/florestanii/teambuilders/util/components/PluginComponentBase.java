package me.florestanii.teambuilders.util.components;

import org.bukkit.Server;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.logging.Logger;

public abstract class PluginComponentBase implements PluginComponent {
    private JavaPlugin plugin;

    @Override
    public final void activateFor(JavaPlugin plugin) {
        this.plugin = plugin;

        if (this instanceof Listener) {
            registerEvents((Listener) this);
        }

        onActivated();
    }

    public final void registerEvents(Listener listener) {
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }

    public final Logger getLogger() {
        return plugin.getLogger();
    }

    public final void registerCommand(String command, CommandExecutor executor) {
        PluginCommand cmd = plugin.getCommand(command);
        if (cmd != null) {
            cmd.setExecutor(executor);
        } else {
            getLogger().warning("Command '" + command + "' is not declared in the plugin.yml so it is not available.");
        }
    }

    public final Configuration getConfig() {
        return plugin.getConfig();
    }

    public final void saveConfig() {
        plugin.saveConfig();
    }

    public final File getDataFolder() {
        return plugin.getDataFolder();
    }

    public Server getServer() {
        return plugin.getServer();
    }

    public void scheduleSyncDelayedTask(Runnable runnable) {
        getServer().getScheduler().scheduleSyncDelayedTask(plugin, runnable);
    }

    public final BukkitTask runTaskTimer(Runnable task, long delay, long period) {
        return getServer().getScheduler().runTaskTimer(plugin, task, delay, period);
    }

    public BukkitTask runTaskAsynchronously(Runnable task) {
        return getServer().getScheduler().runTaskAsynchronously(plugin, task);
    }

    public final BukkitTask runTaskTimerAsynchronously(Runnable task, long delay, long period) {
        return getServer().getScheduler().runTaskTimerAsynchronously(plugin, task, delay, period);
    }

    protected void onActivated() {
    }
}
