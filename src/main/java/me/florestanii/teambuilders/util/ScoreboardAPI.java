package me.florestanii.teambuilders.util;

import java.util.Map;

import me.florestanii.teambuilders.arena.TeamBuildersArena;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardAPI {

	public static void updateBuildingScoreboard(Player p, TeamBuildersArena arena) {
		
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective obj = board.registerNewObjective("info", "info");
		
		obj.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Team" + ChatColor.GREEN + ChatColor.BOLD + "Builders");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Score five = obj.getScore(ChatColor.GOLD + "Time left:");
		int countdown = arena.getBuildTime().getCountdown();
		Score four = obj.getScore("  " + (int)(countdown/60) + ":" + (((countdown%60) + "").length() == 1 ? "0" + (countdown%60) : (countdown%60)) + " min");
		Score three = obj.getScore("          ");
		Score two = obj.getScore(ChatColor.BLUE + "Word:");
		Score one = obj.getScore("  " + arena.getWord());
		
		five.setScore(5);
		four.setScore(4);
		three.setScore(3);
		two.setScore(2);
		one.setScore(1);
		
		p.setScoreboard(board);
		
	}
	
	public static void updatePreparingScoreboard(Player p, int countdown) {
		
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective obj = board.registerNewObjective("info", "info");
		obj.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Team" + ChatColor.GREEN + ChatColor.BOLD + "Builders");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Score five = obj.getScore(ChatColor.GOLD + "Round starts in:");
		Score four = obj.getScore("  " + (int)(countdown/60) + ":" + (((countdown%60) + "").length() == 1 ? "0" + (countdown%60) : (countdown%60)) + " min");
		Score three = obj.getScore("          ");
		Score two = obj.getScore(ChatColor.BLUE + "Word:");
		Score one = obj.getScore("  ");
		
		five.setScore(5);
		four.setScore(4);
		three.setScore(3);
		two.setScore(2);
		one.setScore(1);
		
		p.setScoreboard(board);
		
	}
	
	public static void updateVotingScoreboard(Player p, int countdown, String word) {
		
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective obj = board.registerNewObjective("info", "info");
		obj.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Team" + ChatColor.GREEN + ChatColor.BOLD + "Builders");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Score five = obj.getScore(ChatColor.GOLD + "Round starts in:");
		Score four = obj.getScore("  " + (int)(countdown/60) + ":" + (((countdown%60) + "").length() == 1 ? "0" + (countdown%60) : (countdown%60)) + " min");
		Score three = obj.getScore("          ");
		Score two = obj.getScore(ChatColor.BLUE + "Word:");
		Score one = obj.getScore("  " + word);
		
		five.setScore(5);
		four.setScore(4);
		three.setScore(3);
		two.setScore(2);
		one.setScore(1);
		
		p.setScoreboard(board);
		
	}
	
	public static void showResultsScoreboard(Player p, Map<String, Integer> results) {

		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective obj = board.registerNewObjective("info", "info");
		obj.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Team" + ChatColor.GREEN + ChatColor.BOLD + "Builders");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		for (String name : results.keySet()) {
			Score score = obj.getScore(name);
			score.setScore(results.get(name));
		}
		
		p.setScoreboard(board);
		
	}
	
	public static void clearScoreboard(Player p) {
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		p.setScoreboard(board);
	}
	
}
