package me.florestanii.teambuilders.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtil {
	
	public static ItemStack getItemStack(Material material, String name) {
		
		ItemStack item = new ItemStack(material);
		
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		
		item.setItemMeta(meta);
		
		return item;
		
	}
	
}
