package me.florestanii.teambuilders.util.commands;


public interface CommandHandler {
    enum Result {
        Done,
        CommandNotFound,
        NoPermission
    }
}
