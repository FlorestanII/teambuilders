package me.florestanii.teambuilders.util;

import me.florestanii.teambuilders.util.components.PluginComponentBase;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class WeakPlayerMaps extends PluginComponentBase implements Listener {
    private Collection<Map> playerDataMap = new ArrayList<>();

    public <T> Map<Player, T> createMap(Class<T> clazz) {
        Map<Player, T> map = new HashMap<>();
        playerDataMap.add(map);
        return map;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        for (Map map : playerDataMap) {
            map.remove(event.getPlayer());
        }
    }

    public void reset() {
        for (Map map : playerDataMap) {
            map.clear();
        }
    }
}
