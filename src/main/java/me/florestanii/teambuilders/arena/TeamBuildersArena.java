package me.florestanii.teambuilders.arena;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.lobby.TeamBuildersLobby;
import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;
import me.florestanii.teambuilders.util.ScoreboardAPI;

public class TeamBuildersArena {

	private final TeamBuilders tb;
	
	private String name;
	
	private int teamSize = 0;	
	private int flyHeight = 0;
	
	private int buildHeight = 0;
	private int buildDepth = 0;
	
	private TeamBuildersLobby lobby;
	
	private int minplayers = 0;
	
	private final ArrayList<TeamBuildersTeam> teams = new ArrayList<TeamBuildersTeam>();
		
	private TeamBuildersArenaState state = TeamBuildersArenaState.LOBBY;
	
	String word;
	
	private BuildTime buildTime;
	
	public TeamBuildersArena(TeamBuilders tb) {
		this.tb = tb;
		this.lobby = new TeamBuildersLobby(tb, this);
	}
	
	public boolean isInArena(Player player) {
		for (TeamBuildersTeam team : teams) {
			if (team.isInArena(player)) {
				return true;
			}
		}
		
		return lobby.isInArena(player);
	}
	
	public void joinPlayer(Player player) {
		
		if (getPlayerCount() >= teamSize*teams.size()) {
			player.sendMessage(ChatColor.RED + "This arena is full");
			return;
		}
		
		if (state != TeamBuildersArenaState.LOBBY) {
			player.sendMessage(ChatColor.RED + "This arena is already started!");
			return;
		}
		
		player.setMetadata("teambuilders-inv-backup", new FixedMetadataValue(tb, player.getInventory().getContents()));
		player.setMetadata("teambuilders-armor-backup", new FixedMetadataValue(tb, player.getInventory().getArmorContents()));
		player.setMetadata("teambuilders-lvl-backup", new FixedMetadataValue(tb, player.getLevel()));
		player.setMetadata("teambuilders-exp-backup", new FixedMetadataValue(tb, player.getExp()));
		player.setMetadata("teambuilders-leave-pos-backup", new FixedMetadataValue(tb, player.getLocation()));
		player.setMetadata("teambuilders-game-mode-backup", new FixedMetadataValue(tb, player.getGameMode()));
		player.setMetadata("teambuilders-food-lvl-backup", new FixedMetadataValue(tb, player.getFoodLevel()));
		player.setMetadata("teambuilders-health-backup", new FixedMetadataValue(tb, player.getHealth()));
		
		ScoreboardAPI.clearScoreboard(player);
		
		lobby.join(player);
		
	}
	
	public void leavePlayer(Player player) {
		List<MetadataValue> inventory = player.getMetadata("teambuilders-inv-backup");
        player.removeMetadata("teambuilders-inv-backup", tb);
        if (!inventory.isEmpty()) {
            player.getInventory().setContents((ItemStack[]) inventory.get(0).value());
            player.updateInventory();
        }
        
        List<MetadataValue> armor = player.getMetadata("teambuilders-armor-backup");
        player.removeMetadata("teambuilders-armor-backup", tb);
        if (!armor.isEmpty()) {
            player.getInventory().setArmorContents((ItemStack[]) armor.get(0).value());
            player.updateInventory();
        }
        
        List<MetadataValue> leavePos = player.getMetadata("teambuilders-leave-pos-backup");
        player.removeMetadata("teambuilders-leave-pos-backup", tb);
        player.teleport((Location) leavePos.get(0).value());
        
        List<MetadataValue> level = player.getMetadata("teambuilders-lvl-backup");
        player.removeMetadata("teambuilders-lvl-backup", tb);
        player.setLevel(level.get(0).asInt());
        
        List<MetadataValue> exp = player.getMetadata("teambuilders-exp-backup");
        player.removeMetadata("teambuilders-exp-backup", tb);
        player.setExp(exp.get(0).asFloat());
        
        List<MetadataValue> gamemode = player.getMetadata("teambuilders-game-mode-backup");
        player.removeMetadata("teambuilders-game-mode-backup", tb);
        player.setGameMode((GameMode)gamemode.get(0).value());
        
        List<MetadataValue> foodlvl = player.getMetadata("teambuilders-food-lvl-backup");
        player.removeMetadata("teambuilders-food-lvl-backup", tb);
        player.setFoodLevel(foodlvl.get(0).asInt());
        
        List<MetadataValue> health = player.getMetadata("teambuilders-health-backup");
        player.removeMetadata("teambuilders-health-backup", tb);
        player.setHealth(health.get(0).asFloat());
        
        ScoreboardAPI.clearScoreboard(player);
        
        if (player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) {
        	player.setAllowFlight(false);
        }

		player.resetPlayerTime();
		player.resetPlayerWeather();
		
        if (lobby.isInArena(player)) {
        	lobby.leave(player);
        } else {
        	TeamBuildersTeam team = getTeam(player);
        	
        	if (team != null) {
        		
        		team.leavePlayer(player);
        		
        	}
        	
        }
        
        player.setPlayerListName(player.getName());
        
        if (getPlayerCount() == 0) {
        	reset();
        }
        
	}
	
	public void startArena() {
		
		for (TeamBuildersTeam team : teams) {
			team.getBuildarea().reset();
		}
		
		for (Player player : getPlayers()) {
			player.setGameMode(GameMode.CREATIVE);
			player.getInventory().clear();
			//TODO give options items
		}
		
		LinkedList<Player> players = lobby.getPlayers(); 
		
		while (players.size() > 0) {
			for (TeamBuildersTeam team : teams) {
				if (players.size() == 0) {
					break;
				}
				
				Player player = players.remove(new Random().nextInt(players.size()));
				team.joinPlayer(player);
				
			}
		}
		
		lobby.reset();
		
		state = TeamBuildersArenaState.PREPARING;
		
		new PreparingCountdown(tb, this).startCountdown();
		
	}
	
	public boolean addTeam(TeamBuildersTeam team) {
		boolean exists = false;
		
		for (TeamBuildersTeam t : this.teams) {
			if (t.getTeamName().equals(team.getTeamName()))
				exists = true;
				break;
		}
		
		if (!exists)
			teams.add(team);
		
		return !exists;
		
	}
	
	public TeamBuildersTeam getTeam(Player player) {
		for (TeamBuildersTeam team : teams) {
			if (team.isInArena(player)) {
				return team;
			}
		}
		return null;
	}
	
	public ArrayList<TeamBuildersTeam> getTeams() {
		return teams;
	}
	
	public TeamBuildersTeam getTeam(String name) {
		for (TeamBuildersTeam team : teams) {
			if (team.getTeamName().equals(name)) {
				return team;
			}
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.buildTime = new BuildTime(tb, name);
		this.name = name;
	}
	
	public void setTeamSize(int teamSize) {
		this.teamSize = teamSize;
	}
	
	public int getTeamSize() {
		return teamSize;
	}
	
	public int getFlyHeight() {
		return flyHeight;
	}

	public void setFlyHeight(int flyHeight) {
		this.flyHeight = flyHeight;
	}

	public int getBuildHeight() {
		return buildHeight;
	}

	public void setBuildHeight(int buildHeight) {
		this.buildHeight = buildHeight;
	}

	public int getBuildDepth() {
		return buildDepth;
	}

	public void setBuildDepth(int buildDepth) {
		this.buildDepth = buildDepth;
	}

	public void setLobbySpawn(Location loc) {
		lobby.setSpawn(loc);
	}
	
	public Location getLobbySpawn() {
		return lobby.getSpawn();
	}
	
	public TeamBuildersLobby getLobby() {
		return lobby;
	}
	
	public boolean isReady() {
		
		for (TeamBuildersTeam team : teams) {
			if (!team.isReady()) {
				return false;
			}
		}
		
		return name != null && buildDepth != 0 && flyHeight != 0 && buildHeight != 0 && flyHeight > buildHeight && teamSize != 0 && teams.size() >= 2 && lobby != null && minplayers != 0;
	}
	
	public LinkedList<Player> getPlayers() {
		LinkedList<Player> players = new LinkedList<Player>();
		
		players.addAll(lobby.getPlayers());
		
		for (TeamBuildersTeam team : teams) {
			players.addAll(team.getPlayers());
		}
		
		return players;
	}
	
	public int getPlayerCount() {
		return getPlayers().size();
	}
	
	public void setState(TeamBuildersArenaState state) {
		this.state = state;
	}
	
	public TeamBuildersArenaState getState() {
		return state;
	}
	
	public void setMinPlayers(int minplayers) {
		this.minplayers = minplayers;
	}
	
	public int getMinPlayers() {
		return minplayers;
	}
	
	public BuildTime getBuildTime() {
		return buildTime;
	}
	
	public void reset() {
		lobby.reset();
		buildTime.stop();
		
		for (TeamBuildersTeam team : teams) {
			team.reset();
		}
		
		state = TeamBuildersArenaState.LOBBY;
	}
	
	public String getWord() {
		return word;
	}
	
}
