package me.florestanii.teambuilders.arena.teams;

import java.util.LinkedList;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.BuildArea;
import me.florestanii.teambuilders.arena.TeamBuildersArena;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeamBuildersTeam {

	private final TeamBuilders tb;
	
	private final TeamBuildersArena arena;
	
	private final String default_name;
	private String name;
	
	private ChatColor chatColor;
	
	private Location spawn;
	
	private BuildArea buildarea;
	
	private LinkedList<Player> players = new LinkedList<Player>();
	
	public TeamBuildersTeam(TeamBuilders tb, TeamBuildersArena arena, String name) {
		this.arena = arena;
		this.name = name;
		this.default_name = name;
		this.tb = tb;
	}
	
	public boolean isPlayerInTeam(Player p) {
		return players.contains(p);
	}
	
	public void joinPlayer(Player player) {
		
		if (players.size() >= arena.getTeamSize()) {
			player.sendMessage(ChatColor.RED + "This team is full!");
			return;
		}
		
		if (player.getCustomName() == null) {
			player.setPlayerListName(chatColor + player.getName());
		} else {
			player.setPlayerListName(chatColor + player.getCustomName());
		}
		
		player.teleport(spawn);
		
		player.setPlayerTime(buildarea.getTime(), false);
		player.setPlayerWeather(buildarea.getWeather());
		
		players.add(player);
	}
	
	public void leavePlayer(Player player) {
		players.remove(player);
	}
	
	public String getTeamName() {
		return name;
	}
	
	public TeamBuildersArena getArena() {
		return arena;
	}
	
	public LinkedList<Player> getPlayers() {
		return players;
	}
	
	public void setChatColor(ChatColor chatColor) {
		this.chatColor = chatColor;
	}
	public ChatColor getChatColor() {
		return chatColor;
	}
	
	public void setSpawn(Location loc) {
		this.spawn = loc;
	}
	
	public Location getSpawn() {
		return spawn;
	}
	
	public String getDefaultName() {
		return default_name;
	}
	
	public boolean isInArena(Player player) {
		return players.contains(player);
	}
	
	public boolean isReady() {
		return spawn != null && name != null && chatColor != null && buildarea != null && buildarea.isReady();
	}

	public BuildArea getBuildarea() {
		return buildarea;
	}

	public void setBuildarea(BuildArea buildarea) {
		this.buildarea = buildarea;
	}
	
	public void reset() {
		buildarea.reset();
		
		for (Player p : players) {
			arena.leavePlayer(p);
		}
		
	}
	
}
