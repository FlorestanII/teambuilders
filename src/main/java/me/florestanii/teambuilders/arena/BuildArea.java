package me.florestanii.teambuilders.arena;

import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.World;

public class BuildArea {

	private Location pos1;
	
	private Location pos2;
	
	private final TeamBuildersArena arena;
	
	private final TeamBuildersTeam team;
	
	private long time = 6000;

	private WeatherType weather = WeatherType.CLEAR;
	
	public BuildArea(TeamBuildersArena arena, TeamBuildersTeam team) {
		this.arena = arena;
		this.team = team;
	}

	public Location getPos1() {
		if (pos1 != null && pos2 != null) {
			return new Location(this.pos1.getWorld(), Math.min(this.pos1.getX(), this.pos2.getX()), Math.min(this.pos1.getY(), this.pos2.getY()), Math.min(this.pos1.getZ(), this.pos2.getZ()));
		}
		return pos1;
	}

	public void setPos1(Location pos1) {
		this.pos1 = pos1;
	}

	public Location getPos2() {
		if (pos1 != null && pos2 != null) {
			return new Location(this.pos1.getWorld(), Math.max(this.pos1.getX(), this.pos2.getX()), Math.max(this.pos1.getY(), this.pos2.getY()), Math.max(this.pos1.getZ(), this.pos2.getZ()));
		}
		return pos2;
	}

	public void setPos2(Location pos2) {
		this.pos2 = pos2;
	}
	
	public boolean isInArea(Location loc) {
		
		Location pos1 = getPos1();
		Location pos2 = getPos2();
		
		pos1.setY(pos1.getY() - arena.getBuildDepth() + 1);
		pos2.setY(pos2.getY() + arena.getBuildHeight());
		
		if (loc.getBlockX() >= pos1.getBlockX() && loc.getBlockX() <= pos2.getBlockX()) {
			if (loc.getBlockY() >= pos1.getBlockY() && loc.getBlockY() <= pos2.getBlockY()) {
				if (loc.getBlockZ() >= pos1.getBlockZ() && loc.getBlockZ() <= pos2.getBlockZ()) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		Location pos1 = getPos1();
		Location pos2 = getPos2();
		return "(" + pos1.getX() + "|" + pos1.getY() + "|" + pos1.getZ() + " -> " + pos2.getX() + "|" + pos2.getY() + "|" + pos2.getZ() + ")";
	}
	
	public boolean isReady() {
		return pos1 != null && pos2 != null;
	}
	
	public void reset() {
		Location pos1 = getPos1();
		Location pos2 = getPos2();
		
		World world = pos1.getWorld();
		
		for (int x = (int) pos1.getX(); x <= pos2.getX(); x++) {
			for (int z = (int) pos1.getZ(); z <= pos2.getZ(); z++) {
				
				for (int y = (int) pos1.getY(); y > pos1.getY() - arena.getBuildDepth(); y--) {
					if (y == (int)pos1.getY()) {
						world.getBlockAt(x, y, z).setType(Material.GRASS);
					} else {
						world.getBlockAt(x, y, z).setType(Material.DIRT);
					}
				}
				
				for (int y = (int) pos2.getY() + 1; y < pos2.getY() + 1 + arena.getBuildHeight(); y++) {
					world.getBlockAt(x, y, z).setType(Material.AIR);
				}
				
			}
		}
		
	}
	
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public WeatherType getWeather() {
		return weather;
	}

	public void setWeather(WeatherType weather) {
		this.weather = weather;
	}
	
	public TeamBuildersTeam getTeam() {
		return team;
	}
	
}
