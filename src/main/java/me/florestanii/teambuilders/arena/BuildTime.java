package me.florestanii.teambuilders.arena;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;
import me.florestanii.teambuilders.util.ScoreboardAPI;
import me.florestanii.teambuilders.util.TitleAPI;

public class BuildTime {

	private final TeamBuilders tb;
	private final String arena;
	
	private int countdownScheduler = -1;

	private final int startCountdown = 60; //TODO save in config
	private int countdown = startCountdown;
	
	public BuildTime(TeamBuilders tb, String arena) {
		this.tb = tb;
		this.arena = arena;
	}
	
	public void start() {
		countdownScheduler = tb.getServer().getScheduler().scheduleSyncRepeatingTask(tb, new Runnable() {
			
			@Override
			public void run() {
				
				if (countdown != 0) {
					
					if (countdown % 60 == 0 || countdown <= 10) {
						broadcastMessage(ChatColor.RED + "" + countdown + ChatColor.YELLOW + " seconds remaining!");
						broadcastNote(Instrument.BASS_GUITAR, new Note(10));
					}
					
					for (Player p : tb.getArena(arena).getPlayers()) {
						ScoreboardAPI.updateBuildingScoreboard(p, tb.getArena(arena));
					}
					
					countdown--;
					
				} else {
					stop();
					
					tb.getArena(arena).setState(TeamBuildersArenaState.VOTING);
					
					broadcastMessage(ChatColor.YELLOW + "Time is up!");
					
					for (Player p : tb.getArena(arena).getPlayers()) {
						p.setGameMode(GameMode.SURVIVAL);
						p.getInventory().clear();
						p.setAllowFlight(true);
						ScoreboardAPI.clearScoreboard(p);
					}
					
					ArrayList<TeamBuildersTeam> teams = tb.getArena(arena).getTeams();
					
					BuildArea[] buildareas = new BuildArea[teams.size()];
					
					for (int i = 0; i < teams.size(); i++) {
						buildareas[i] = teams.get(i).getBuildarea();
					}
					
					new VotingPhase(tb, tb.getArena(arena), buildareas);
					
				}
				
			}
		}, 0, 20);
	}
	
	public void stop() {
		tb.getServer().getScheduler().cancelTask(countdownScheduler);
		countdownScheduler = -1;
		countdown = startCountdown;
	}
	
	public void broadcastMessage(String message) {
		for (Player p : tb.getArena(arena).getPlayers()) {
			p.sendMessage(message);
			TitleAPI.sendTitle(p, 0, 3, 3, message);
		}
	}
	
    public void broadcastNote(Instrument instrument, Note note) {
        for (Player player : tb.getArena(arena).getPlayers()) {
            player.playNote(player.getLocation(), instrument, note);
        }
    }

	public TeamBuildersArena getArena() {
		return tb.getArena(arena);
	}
	
	public int getCountdown() {
		return countdown;
	}

}
