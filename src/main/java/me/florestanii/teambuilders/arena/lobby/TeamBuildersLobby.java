package me.florestanii.teambuilders.arena.lobby;

import java.util.LinkedList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.arena.TeamBuildersArena;

public class TeamBuildersLobby {

	private final TeamBuilders tb;
	
	private TeamBuildersArena arena;
	
	private Location spawn;
	
	private LinkedList<Player> players = new LinkedList<Player>();
	
	int countdownScheduler = -1;
	final int startCountdown = 30;
	int countdown = startCountdown;
	
	public TeamBuildersLobby(TeamBuilders tb, TeamBuildersArena arena) {
		this.tb = tb;
		this.arena = arena;
	}
	
	public TeamBuildersArena getArena() {
		return arena;
	}
	
	public void setSpawn(Location spawn) {
		this.spawn = spawn;
	}
	
	public Location getSpawn() {
		return spawn;
	}
	
	public LinkedList<Player> getPlayers() {
		return players;
	}

	public boolean isInArena(Player player) {
		return players.contains(player);
	}
	
	public void join(Player player) {
		player.getInventory().clear();
		player.getInventory().setArmorContents(new ItemStack[] {new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
		player.setGameMode(GameMode.SURVIVAL);
		player.setLevel(0);
		player.setExp(0);
		player.teleport(spawn);
		player.setFoodLevel(20);
		player.setHealth(20);
		
		player.setPlayerTime(6000, false);
		player.setPlayerWeather(WeatherType.CLEAR);
		
		players.add(player);

		int playercount = arena.getPlayerCount();
		
		if (playercount >= arena.getMinPlayers()) {
			startCountdown();
		}

		if (player.getCustomName() == null) {
			player.setPlayerListName(ChatColor.GREEN + player.getName());
		} else {
			player.setPlayerListName(ChatColor.GREEN + player.getCustomName());
		}
		
	}
	
	public void leave(Player player) {
		players.remove(player);
		
        int playercount = arena.getPlayerCount();
        
        if (playercount < arena.getMinPlayers()) {
        	stopCountdown();
        }
		
	}
	
	public void startCountdown() {
		countdownScheduler = tb.getServer().getScheduler().scheduleSyncRepeatingTask(tb, new Runnable() {
			
			@Override
			public void run() {
				for (Player p : players) {
					p.setLevel(countdown);
				}
				
				if (countdown != 0) {
					
					if (countdown == startCountdown || countdown % 10 == 0 || countdown <= 10) {
						broadcastMessage(ChatColor.YELLOW + "The arena starts in " + ChatColor.RED + countdown + ChatColor.YELLOW + " seconds!");
					}

					if (countdown <= 10) {
						broadcastNote(Instrument.BASS_GUITAR, new Note(10));
					}
					
					countdown--;
					
				} else {
					stopCountdown();
					
					arena.startArena();
					
				}
				
			}
		}, 0, 20);
	}
	
	public void stopCountdown() {
		tb.getServer().getScheduler().cancelTask(countdownScheduler);
		countdownScheduler = -1;
		countdown = startCountdown;
	}
	
	public void broadcastMessage(String message) {
		for (Player p : players) {
			p.sendMessage(message);
		}
	}
	
    public void broadcastNote(Instrument instrument, Note note) {
        for (Player player : players) {
            player.playNote(player.getLocation(), instrument, note);
        }
    }
    
    public void reset() {
    	stopCountdown();
    	for (Player p : players) {
    		arena.leavePlayer(p);
    	}
    }
    
    public void setCountdown(int countdown) {
    	this.countdown = countdown;
    }
    
}
