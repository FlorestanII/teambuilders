package me.florestanii.teambuilders.arena;

public enum TeamBuildersArenaState {
	LOBBY, PREPARING, BUILDING, VOTING
}
