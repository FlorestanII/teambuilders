package me.florestanii.teambuilders.arena;

import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.util.ScoreboardAPI;
import me.florestanii.teambuilders.util.TitleAPI;

public class PreparingCountdown {

	private final TeamBuilders tb;
	
	private TeamBuildersArena arena;
	
	private int countdownScheduler = -1;
	private final int startCountdown = 10;
	private int countdown = startCountdown;
	
	public PreparingCountdown(TeamBuilders tb, TeamBuildersArena arena) {
		this.tb = tb;
		this.arena = arena;
	}
	
	public void startCountdown() {
		countdownScheduler = tb.getServer().getScheduler().scheduleSyncRepeatingTask(tb, new Runnable() {
			
			@Override
			public void run() {
				for (Player p : arena.getPlayers()) {
					p.setLevel(countdown);
				}
				
				if (countdown != 0) {
					
					broadcastMessage(ChatColor.YELLOW + "The arena starts in " + ChatColor.RED + countdown + ChatColor.YELLOW + " seconds!");
					broadcastNote(Instrument.BASS_GUITAR, new Note(10));
					
					countdown--;
					
				} else {
					stopCountdown();
					
					arena.setState(TeamBuildersArenaState.BUILDING);
					String word = tb.getRandomWord();
					arena.word = word;
					
					for (Player p : arena.getPlayers()) {
						TitleAPI.sendTitle(p, 0, 7, 3, word);
						p.sendMessage(ChatColor.YELLOW + "The word is: " + ChatColor.RED + word);
					}
					arena.getBuildTime().start();
				}
				
			}
		}, 0, 20);
	}
	
	public void stopCountdown() {
		tb.getServer().getScheduler().cancelTask(countdownScheduler);
		countdownScheduler = -1;
		countdown = startCountdown;
	}
	
	public TeamBuildersArena getArena() {
		return arena;
	}
	
	public void broadcastMessage(String message) {
		for (Player p : arena.getPlayers()) {
			p.sendMessage(message);
			ScoreboardAPI.updatePreparingScoreboard(p, countdown);
		}
	}
	
    public void broadcastNote(Instrument instrument, Note note) {
        for (Player player : arena.getPlayers()) {
            player.playNote(player.getLocation(), instrument, note);
        }
    }
    
 
}
