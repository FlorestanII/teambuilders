package me.florestanii.teambuilders.arena;

import java.util.Map;

import me.florestanii.teambuilders.TeamBuilders;

import org.bukkit.entity.Player;

public class ArenaSetupCache {

	private final TeamBuilders tb;
	
	private static Map<Player, TeamBuildersArena> cache;
	
	public ArenaSetupCache(TeamBuilders tb) {
		this.tb = tb;
		cache = this.tb.getWeakPlayerMaps().createMap(TeamBuildersArena.class);
	}
	
	public static TeamBuildersArena getCacheOfPlayer(Player p) {
		return cache.get(p);
	}
	
	public static void removeCache(Player p) {
		cache.remove(p);
	}
	
	public static void setCache(Player p, TeamBuildersArena arena) {
		cache.put(p, arena);
	}
	
}
