package me.florestanii.teambuilders.arena;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.florestanii.teambuilders.TeamBuilders;
import me.florestanii.teambuilders.util.ItemUtil;
import me.florestanii.teambuilders.util.ScoreboardAPI;
import me.florestanii.teambuilders.util.TitleAPI;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class VotingPhase {
	
	private final TeamBuilders tb;
	
	private final TeamBuildersArena arena;
	
	private final LinkedList<BuildArea> areas;
	
	int votingTime = 10;
	int countdown = votingTime;
	int countdownScheduler = -1;
	
	private HashMap<String, Map<Player, Integer>> votes = new HashMap<String, Map<Player, Integer>>();
	
	public VotingPhase(TeamBuilders tb, TeamBuildersArena arena, BuildArea[] buildareas) {
		this.tb = tb;
		this.arena = arena;
		this.areas = new LinkedList<BuildArea>();
		for (BuildArea area : buildareas) {
			this.areas.add(area);
		}
		
		tb.getServer().getScheduler().scheduleSyncDelayedTask(tb, new Runnable() {
			
			@Override
			public void run() {
				vote(areas.remove(new Random().nextInt(areas.size())));
			}
		}, 40);
		
	}
	
	public void vote(final BuildArea area) {
		for (Player p : area.getTeam().getArena().getPlayers()) {
			p.teleport(area.getPos2().add(1, arena.getBuildHeight(), 1));
			p.setPlayerWeather(area.getWeather());
			p.setPlayerTime(area.getTime(), false);
			giveVoteInv(p);
		}
		
		final Listener lis = new Listener() {
			@EventHandler(priority=EventPriority.MONITOR)
			public void onInteract(PlayerInteractEvent event) {
				
				TeamBuildersArena arena = tb.getArena(event.getPlayer());
				
				if (arena != null) {
					
					if (arena.getState() == TeamBuildersArenaState.VOTING) {
						
						ItemStack item = event.getItem();
						
						if (item != null) {
							
							String name = item.getItemMeta().getDisplayName();
							
							if (name != null) {
								
								event.getPlayer().updateInventory();
								
								if (votes.get(area.getTeam().getTeamName()) == null) {
									votes.put(area.getTeam().getTeamName(), tb.getWeakPlayerMaps().createMap(Integer.class));
								}
								
								int vote = 0;
								
								if (name.equals(ChatColor.GREEN + "+1")) {
									vote = 1;
								} else if (name.equals(ChatColor.GREEN + "+2")) {
									vote = 2;
								} else if (name.equals(ChatColor.GREEN + "+5")) {
									vote = 5;
								} else if (name.equals(ChatColor.RED + "-1")) {
									vote = -1;
								} else if (name.equals(ChatColor.RED + "-2")) {
									vote = -2;
								} else if (name.equals(ChatColor.RED + "-5")) {
									vote = -5;
								}
								
								if (vote != 0) {
									
									if (area.getTeam().isInArena(event.getPlayer())) {
										event.getPlayer().sendMessage(ChatColor.RED + "You can't vote for your own team!");
										return;
									}
									
								}
								
								Integer oldVote = votes.get(area.getTeam().getTeamName()).get(event.getPlayer());
								
								if (oldVote != null) {
									oldVote = oldVote + vote;
								} else {
									oldVote = vote;
								}
								
								if (oldVote < 0) {
									oldVote = 0;
								}
								
								if (oldVote > 15) {
									oldVote = 15;
								}
								
								event.getPlayer().sendMessage(ChatColor.YELLOW + "Your vote: " + ChatColor.RED + oldVote + "/15" + ChatColor.YELLOW + " points.");
								TitleAPI.sendTitle(event.getPlayer(), 0, 2, 1, ChatColor.YELLOW + "Your vote: " + ChatColor.RED + oldVote + "/15" + ChatColor.YELLOW + " points.");
								votes.get(area.getTeam().getTeamName()).put(event.getPlayer(), oldVote);
								
							}
							
						}
						
					}
					
				}
				
			}
			
		};
		
		tb.getServer().getPluginManager().registerEvents(lis, tb);
		
		tb.getServer().getScheduler().scheduleSyncDelayedTask(tb, new Runnable() {
			@Override
			public void run() {
				PlayerInteractEvent.getHandlerList().unregister(lis);
			}
		}, votingTime*20);
		
		countdownScheduler = tb.getServer().getScheduler().scheduleSyncRepeatingTask(tb, new Runnable() {
			
			@Override
			public void run() {
				
				if (countdown != 0) {
					
					for (Player p : area.getTeam().getArena().getPlayers()) {
						ScoreboardAPI.updateVotingScoreboard(p, countdown, arena.getWord());
					}
					
					countdown--;
					
				} else {
					
					tb.getServer().getScheduler().cancelTask(countdownScheduler);
					countdown = votingTime;
					
					for (Player p : area.getTeam().getArena().getPlayers()) {
						TitleAPI.sendTitle(p, 0, 2, 2, "Build by: " + area.getTeam().getTeamName());
					}
					
					tb.getServer().getScheduler().scheduleSyncDelayedTask(tb, new Runnable() {
						
						@Override
						public void run() {
							if (areas.size() > 0) {
								vote(areas.remove(new Random().nextInt(areas.size())));
							} else {
								for (Player p : area.getTeam().getArena().getPlayers()) {
									ScoreboardAPI.clearScoreboard(p);
								}
								
								Map<String, Integer> results = new HashMap<String, Integer>();
								
								for (String name : votes.keySet()) {
									results.put(name, 0);
									
									Map<Player, Integer> votesForTeam = votes.get(name);
									
									int vote = 0;
									
									for (Integer i : votesForTeam.values()) {
										vote += i;
									}
									
									results.put(name, vote);
									
								}
								
								results = sortByValue(results);
								
								
								
								String winnerTeamName = new ArrayList<>(results.entrySet()).get(results.size()-1).getKey();
								
								for (Player p : arena.getPlayers()) {
									ScoreboardAPI.showResultsScoreboard(p, results);
									TitleAPI.sendTitle(p, 0, 10, 0, "Team " + winnerTeamName + " won the game!");
									p.teleport(arena.getTeam(winnerTeamName).getSpawn().add(0, arena.getFlyHeight(), 0));
								}
								
								tb.getServer().getScheduler().scheduleSyncDelayedTask(tb, new Runnable() {
									
									@Override
									public void run() {
										
										for (Player p : arena.getPlayers()) {
											arena.leavePlayer(p);
										}
										
									}
								}, 200);
								
							}
						}
					}, 40);
					
				}
				
			}
		}, 0, 20);
		
		
	}
	
	private void giveVoteInv(Player p) {
		
		ItemStack add1 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.GREEN + "+1");
		ItemStack add2 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.GREEN + "+2");
		ItemStack add5 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.GREEN + "+5");
		ItemStack del1 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.RED + "-1");
		ItemStack del2 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.RED + "-2");
		ItemStack del5 = ItemUtil.getItemStack(Material.STONE_BUTTON, ChatColor.RED + "-5");
		
		p.getInventory().setItem(0, del5);
		p.getInventory().setItem(1, del2);
		p.getInventory().setItem(2, del1);
		p.getInventory().setItem(6, add1);
		p.getInventory().setItem(7, add2);
		p.getInventory().setItem(8, add5);
		
		p.updateInventory();
		
	}
	
	public TeamBuildersArena getArena() {
		return arena;
	}
	
	 public <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ) {
		 List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
		 Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
			 public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
				 return (o1.getValue()).compareTo( o2.getValue() );
			 }
		 } );

		 Map<K, V> result = new LinkedHashMap<K, V>();
		 for (Map.Entry<K, V> entry : list) {
			 result.put( entry.getKey(), entry.getValue() );
		 }
		 return result;
	 }
}
