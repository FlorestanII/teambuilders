package me.florestanii.teambuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import me.florestanii.teambuilders.arena.ArenaSetupCache;
import me.florestanii.teambuilders.arena.BuildArea;
import me.florestanii.teambuilders.arena.TeamBuildersArena;
import me.florestanii.teambuilders.arena.teams.TeamBuildersTeam;
import me.florestanii.teambuilders.commands.ArenaCommands;
import me.florestanii.teambuilders.commands.ArenaSetupCommands;
import me.florestanii.teambuilders.commands.TeamBuildersCommandHandler;
import me.florestanii.teambuilders.commands.TeamBuildersSetupCommandHandler;
import me.florestanii.teambuilders.listener.ProtectionHandler;
import me.florestanii.teambuilders.listener.PlayerInventoryBackupHandler;
import me.florestanii.teambuilders.util.ConfigUtil;
import me.florestanii.teambuilders.util.WeakPlayerMaps;
import me.florestanii.teambuilders.util.commands.SubCommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class TeamBuilders extends JavaPlugin{
	
	private final WeakPlayerMaps weakPlayerMaps;
	
	private final ArrayList<TeamBuildersArena> arenas = new ArrayList<TeamBuildersArena>();
	
	private List<String> words = new ArrayList<String>();
	
	public TeamBuilders() {
		weakPlayerMaps = new WeakPlayerMaps();	
	}
	
	@Override
	public void onLoad() {
		
	}
	
	@Override
	public void onEnable() {
		
		saveDefaultConfig();
		
		loadArenasFromConfig();
		
		words = getConfig().getStringList("words");
		
		weakPlayerMaps.activateFor(this);
		new ArenaSetupCache(this);
		
		SubCommandHandler commandHandler = new TeamBuildersCommandHandler();
		commandHandler.addHandlers(new ArenaCommands(this));
		getCommand("teambuilders").setExecutor(commandHandler);
		
		SubCommandHandler setupCommandHandler = new TeamBuildersSetupCommandHandler();
		setupCommandHandler.addHandlers(new ArenaSetupCommands(this));
		getCommand("teambuilderssetup").setExecutor(setupCommandHandler);
		
		new PlayerInventoryBackupHandler(this).activateFor(this);
		new ProtectionHandler(this).activateFor(this);
		
	}
	
	@Override
	public void onDisable() {
		for (TeamBuildersArena arena : arenas) {
			for (Player p : arena.getPlayers()) {
				arena.leavePlayer(p);
			}
		}
		
		saveConfig();
		
	}
	
	public WeakPlayerMaps getWeakPlayerMaps() {
		return weakPlayerMaps;
	}
	
	public TeamBuildersArena getArena(Player player) {
		for (TeamBuildersArena arena : arenas) {
			if (arena.isInArena(player)) {
				return arena;
			}
		}
		return null;
	}
	
	public ArrayList<TeamBuildersArena> getArenas() {
		return arenas;
	}
	
	public boolean exists(TeamBuildersArena arena) {
		
		for (TeamBuildersArena a : arenas) {
			if (arena.getName().equals(a.getName())) {
				return true;
			}
		}
		
		return false;
	}
	
	public void addArena(TeamBuildersArena arena) {
		
		if (!exists(arena)) {
			arenas.add(arena);
		}
		
	}
	
	public TeamBuildersArena getArena(String name) {
		for (TeamBuildersArena arena : arenas) {
			if (arena.getName().equals(name)) {
				return arena;
			}
		}
		return null;
	}
	
	public void saveArena(TeamBuildersArena arena) {
		
		ConfigurationSection arenasSection = null;
		
		if (!getConfig().isConfigurationSection("arenas"))
			arenasSection = getConfig().createSection("arenas");
			
		arenasSection = getConfig().getConfigurationSection("arenas");
		
		ConfigurationSection arenaSection = arenasSection.createSection(arena.getName());
		
		arenaSection.set("name", arena.getName());
		arenaSection.set("teamSize", arena.getTeamSize());
		arenaSection.set("flyHeight", arena.getFlyHeight());
		arenaSection.set("buildHeight", arena.getBuildHeight());
		arenaSection.set("buildDepth", arena.getBuildDepth());
		arenaSection.set("minPlayers", arena.getMinPlayers());
		ConfigUtil.saveLocation(arenaSection.createSection("lobby"), arena.getLobbySpawn());
		
		ConfigurationSection teamSection = arenaSection.createSection("teams");
		
		for (TeamBuildersTeam team : arena.getTeams()) {
			saveTeam(teamSection.createSection(team.getDefaultName()), team);
		}
		
		saveConfig();
		
	}
	
	public void saveBuildArea(ConfigurationSection sec, BuildArea area) {
		ConfigUtil.saveLocation(sec.createSection("pos1"), area.getPos1());
		ConfigUtil.saveLocation(sec.createSection("pos2"), area.getPos2());
	}
	
	public void saveTeam(ConfigurationSection sec, TeamBuildersTeam team) {
		sec.set("default_name", team.getDefaultName());
		ConfigUtil.saveLocation(sec.createSection("spawn"), team.getSpawn());
		sec.set("chatColor", team.getChatColor().name());
		saveBuildArea(sec.createSection("area"), team.getBuildarea());
	}
	
	
	public void loadArenasFromConfig() {
		ConfigurationSection arenasSection = getConfig().getConfigurationSection("arenas");
		
		if (arenasSection == null) {
			getConfig().createSection("arenas");
			return;
		}
		
		for (String key : arenasSection.getKeys(false)) {
			ConfigurationSection arenaSection = arenasSection.getConfigurationSection(key);
			TeamBuildersArena arena = new TeamBuildersArena(this);
			
			arena.setName(arenaSection.getString("name"));
			arena.setTeamSize(arenaSection.getInt("teamSize"));
			arena.setFlyHeight(arenaSection.getInt("flyHeight"));
			arena.setBuildHeight(arenaSection.getInt("buildHeight"));
			arena.setBuildDepth(arenaSection.getInt("buildDepth"));
			arena.setMinPlayers(arenaSection.getInt("minPlayers"));
			arena.setLobbySpawn(ConfigUtil.loadLocation(arenaSection.getConfigurationSection("lobby")));
			
			for (String teamKey : arenaSection.getConfigurationSection("teams").getKeys(false)) {
				ConfigurationSection teamSection = arenaSection.getConfigurationSection("teams").getConfigurationSection(teamKey);
				
				TeamBuildersTeam team = new TeamBuildersTeam(this, arena, teamSection.getString("default_name"));
				team.setSpawn(ConfigUtil.loadLocation(teamSection.getConfigurationSection("spawn")));
				team.setChatColor(ChatColor.valueOf(teamSection.getString("chatColor")));
				
				BuildArea area = new BuildArea(arena, team);
				area.setPos1(ConfigUtil.loadLocation(teamSection.getConfigurationSection("area").getConfigurationSection("pos1")));
				area.setPos2(ConfigUtil.loadLocation(teamSection.getConfigurationSection("area").getConfigurationSection("pos2")));
				team.setBuildarea(area);
				
				arena.addTeam(team);
			}
			
			addArena(arena);
			
		}
		
	}
	
	public void addWord(String word) {
		words.add(word);
		getConfig().set("words", words);
		saveConfig();
	}
	
	public String getRandomWord() {
		if (words.isEmpty()) {
			return "Empty";
		}
		return words.get(new Random().nextInt(words.size()));
	}
	
}
